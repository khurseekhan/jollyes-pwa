import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { PaypalModule } from './store'
import { beforeRegistration } from './hooks/beforeRegistration'

export const KEY = 'paypal'

export const PaymentPaypalModule: StorefrontModule = function ({ store, router, appConfig }) {
  store.registerModule(KEY, PaypalModule)
  beforeRegistration(appConfig, store)
}

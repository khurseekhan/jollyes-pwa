import { Module } from 'vuex'
import { PaypalState } from '../types/PaypalState'
import { getters } from './getters'
import { actions } from './actions'

export const PaypalModule: Module<PaypalState, any> = {
  namespaced: true,
  state: {
    trans: null
  },
  actions,
  getters
}

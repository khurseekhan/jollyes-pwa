import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { QuickLinksModuleStore } from './store/quicklinks/index';

export const QuickLinksModule: StorefrontModule = function ({
  app,
  store,
  router,
  moduleConfig,
  appConfig
}) {
  store.registerModule('quicklinks', QuickLinksModuleStore)
};

import { Module } from 'vuex'
import QuickLinksState from '../../types/QuickLinksState'
import actions from './actions'
import { mutations } from './mutations'
import { getters } from './getters'
export const QuickLinksModuleStore: Module<QuickLinksState, any> = {
  namespaced: true,
  state: {
    quicklinks: []
  },
  mutations,
  actions,
  getters
}

import { MutationTree } from 'vuex'
import * as types from './mutation-types'

export const mutations: MutationTree<any> = {
  [types.QUICKLINKS_FETCH] (state, quicklinks) {
    state.quicklinks = quicklinks || []
  }
}

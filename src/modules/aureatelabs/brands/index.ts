import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { BrandModuleStore } from './store/brand/index';

export const BrandModule: StorefrontModule = function ({
  app,
  store,
  router,
  moduleConfig,
  appConfig
}) {
  store.registerModule('brand', BrandModuleStore)
};

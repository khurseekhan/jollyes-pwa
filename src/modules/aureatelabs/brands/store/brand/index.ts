import { Module } from 'vuex'
import BrandState from '../../types/BrandState'
import actions from './actions'
import { mutations } from './mutations'
import { getters } from './getters'
export const BrandModuleStore: Module<BrandState, any> = {
  namespaced: true,
  state: {
    brands: []
  },
  mutations,
  actions,
  getters
}

import { ActionTree } from 'vuex';
import BrandState from '../../types/BrandState';
import * as types from './mutation-types';
import { Logger } from '@vue-storefront/core/lib/logger';
import { quickSearchByQuery } from '@vue-storefront/core/lib/search';
import { SearchQuery } from 'storefront-query-builder'
import config from 'config';

const alBannerEntityName = config.aureatelabs.brand_key;

const actions: ActionTree<BrandState, any> = {
  /**
   * Retrieve brands
   *
   * @param context
   * @param {any} filterValues
   * @param {any} filterField
   * @param {any} size
   * @param {any} start
   * @param {any} excludeFields
   * @param {any} includeFields
   * @returns {Promise<T> & Promise<any>}
   */

  brandlist (
    context,
    {
      filterValues = null,
      filterField = 'id',
      size = 150,
      start = 0,
      excludeFields = null,
      includeFields = null,
      skipCache = false
    }
  ) {
    let query = new SearchQuery();
    if (filterValues) {
      query = query.applyFilter({
        key: filterField,
        value: { like: filterValues }
      });
    }
    if (
      skipCache ||
      !context.state.brands ||
      context.state.brands.length === 0
    ) {
      return quickSearchByQuery({
        query,
        entityType: alBannerEntityName,
        excludeFields,
        includeFields
      })
        .then(resp => {
          context.commit(types.BRAND_FETCH_BRAND, resp.items);
          return resp.items;
        })
        .catch(err => {
          Logger.error(err, 'brands')();
        });
    } else {
      return new Promise((resolve, reject) => {
        let resp = context.state.brands;
        resolve(resp);
      });
    }
  }
};
export default actions;

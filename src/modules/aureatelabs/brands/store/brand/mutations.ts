import { MutationTree } from 'vuex'
import * as types from './mutation-types'

export const mutations: MutationTree<any> = {
  [types.BRAND_FETCH_BRAND] (state, brands) {
    state.brands = brands || []
  }
}

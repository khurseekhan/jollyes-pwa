import { ActionTree } from 'vuex';
import ourBrandState from '../../types/ourBrandState';
import * as types from './mutation-types';
import { Logger } from '@vue-storefront/core/lib/logger';
import { adjustMultistoreApiUrl } from '@vue-storefront/core/lib/multistore'
import rootStore from '@vue-storefront/core/store'
import i18n from '@vue-storefront/i18n'

const actions: ActionTree<ourBrandState, any> = {
  /**
   * Retrieve brands
   *
   * @param context
   * @param {any} filterValues
   * @param {any} filterField
   * @param {any} size
   * @param {any} start
   * @param {any} excludeFields
   * @param {any} includeFields
   * @returns {Promise<T> & Promise<any>}
   */

  async getOurBrandslist (context, payload) {
    try {
      let url = rootStore.state.config.aureatelabs.ourBrand.endpoint
      if (rootStore.state.config.storeViews.multistore) {
        url = adjustMultistoreApiUrl(url)
      }
      Logger.info('Magento 2 REST API Request with Request Data', 'get-country')()
      await fetch(url + '/' + payload, {
        method: 'GET',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        }
      })
        .then(response => response.json())
        .then(data => {
          if (data.code === 200) {
            Logger.info('Magento 2 REST API Response Data', 'get-country', { data })()

            if (data.code === 200) {
              context.commit(types.OUR_BRANDS_FETCH, data.result)
            }
          } else {
            Logger.error('Something went wrong. Try again in a few seconds.', 'get-country')()
          }
        })
    } catch (e) {
      Logger.error('Something went wrong. Try again in a few seconds.', 'get-country')()
    }
  }
};
export default actions;

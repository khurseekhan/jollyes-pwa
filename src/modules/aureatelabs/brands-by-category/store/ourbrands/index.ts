import { Module } from 'vuex'
import ourBrandState from '../../types/ourBrandState'
import actions from './actions'
import { mutations } from './mutations'
import { getters } from './getters'

export const OurBrandModuleStore: Module<ourBrandState, any> = {
  namespaced: true,
  state: {
    ourbrands: []
  },
  mutations,
  actions,
  getters
}

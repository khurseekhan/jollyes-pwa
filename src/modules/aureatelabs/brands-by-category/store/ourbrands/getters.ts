import ourBrandState from '../../types/ourBrandState'
import { GetterTree } from 'vuex';

export const getters: GetterTree<ourBrandState, any> = {
  getOurBrandslist: (state) => state.ourbrands
}

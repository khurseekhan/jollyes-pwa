import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { OurBrandModuleStore } from './store/ourbrands/index';

export const OurBrandModule: StorefrontModule = function ({
  app,
  store,
  router,
  moduleConfig,
  appConfig
}) {
  store.registerModule('ourbrands', OurBrandModuleStore)
};

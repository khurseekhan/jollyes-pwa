import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { BestSellingModuleStore } from './store/bestSelling/index';

export const BestSellingModule: StorefrontModule = function ({
  app,
  store,
  router,
  moduleConfig,
  appConfig
}) {
  store.registerModule('bestSelling', BestSellingModuleStore)
};

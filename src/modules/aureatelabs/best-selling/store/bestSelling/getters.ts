import SellingState from '../../types/BestSellingState'
import { GetterTree } from 'vuex';

export const getters: GetterTree<SellingState, any> = {
  getBestSellinglist: (state) => state.selling
}

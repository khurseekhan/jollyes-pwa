import { MutationTree } from 'vuex'
import * as types from './mutation-types'

export const mutations: MutationTree<any> = {
  [types.BESTSELLING_FETCH] (state, selling) {
    state.selling = selling || []
  }
}

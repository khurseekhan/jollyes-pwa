import { Module } from 'vuex'
import BannerState from '../../types/BestSellingState'
import actions from './actions'
import { mutations } from './mutations'
import { getters } from './getters'
export const BestSellingModuleStore: Module<BannerState, any> = {
  namespaced: true,
  state: {
    selling: []
  },
  mutations,
  actions,
  getters
}

import { ActionTree } from 'vuex';
import BannerState from '../../types/BestSellingState';
import * as types from './mutation-types';
import { Logger } from '@vue-storefront/core/lib/logger';
import { adjustMultistoreApiUrl } from '@vue-storefront/core/lib/multistore'
import i18n from '@vue-storefront/i18n'
import rootStore from '@vue-storefront/core/store'

const actions: ActionTree<BannerState, any> = {
  async bestSelling (context) {
    try {
      let url = rootStore.state.config.aureatelabs.bestsellingproduct.endpoint
      if (rootStore.state.config.storeViews.multistore) {
        url = adjustMultistoreApiUrl(url)
      }
      Logger.info('Magento 2 REST API Request with Request Data', 'best-selling')()
      await fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        }
      })
        .then(response => response.json())
        .then(data => {
          if (data.code === 200) {
            Logger.info('Magento 2 REST API Response Data', 'best-selling', { data })()
            if (data.code === 200) {
              context.commit(types.BESTSELLING_FETCH, data.result)
            } else {
              rootStore.dispatch('notification/spawnNotification', {
                type: 'error',
                message: data.result.message,
                action1: { label: i18n.t('OK') }
              })
            }
          } else {
            Logger.error('Something went wrong. Try again in a few seconds.', 'best-selling')()
          }
        })
    } catch (e) {
      Logger.error('Something went wrong. Try again in a few seconds.', 'best-selling')()
    }
  }
};
export default actions;
